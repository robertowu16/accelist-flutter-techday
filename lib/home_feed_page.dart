import 'package:flutter/material.dart';
import './utilities/avatar_widget.dart';
import './utilities/post_widget.dart';
import './utilities/models.dart';
import './utilities/ui_utils.dart';

class HomeFeedPage extends StatefulWidget {
  final ScrollController scrollController;

  HomeFeedPage({this.scrollController});

  @override
  _HomeFeedPageState createState() => _HomeFeedPageState();
}

class _HomeFeedPageState extends State<HomeFeedPage> {
  final _posts = <Post>[
    Post(
      user: tie,
      imageUrls: [
        'assets/images/koleksi1.jpg',
        'assets/images/koleksi2.jpg',
        'assets/images/koleksi3.jpg',
      ],
      likes: [
        Like(user: roberto),
        Like(user: hanan),
        Like(user: tie),
      ],
      comments: [
        Comment(
          text: 'Asik nih agan koleksinya',
          user: hanan,
          commentedAt: DateTime(2020, 1, 13, 14, 35, 0),
          likes: [Like(user: accelist)],
        ),
      ],
      location: 'Kamar',
      postedAt: DateTime(2020, 1, 13, 12, 35, 0),
    ),
    Post(
      user: roberto,
      imageUrls: ['assets/images/robertoPost.jpg'],
      likes: [],
      comments: [],
      location: 'Dojo',
      postedAt: DateTime(2020, 1, 11, 6, 0, 0),
    ),
    Post(
      user: nako,
      imageUrls: ['assets/images/nakoPost.jpg'],
      likes: [Like(user: accelist)],
      comments: [],
      location: 'South Korea',
      postedAt: DateTime(2020, 1, 2, 0, 0, 0),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (ctx, i) {
        if (i == 0) {
          return StoriesBarWidget();
        }
        return PostWidget(_posts[i - 1]);
      },
      itemCount: _posts.length + 1,
      controller: widget.scrollController,
    );
  }
}

class StoriesBarWidget extends StatelessWidget {
  final _users = <User>[
    currentUser,
    nako,
    mina,
    roberto,
    tie,
    hanan,
  ];

  void _onUserStoryTap(BuildContext context, int i) {
    final message =
        i == 0 ? 'Add to Your Story' : "View ${_users[i].name}'s Story";
    showSnackbar(context, message);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 106.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (ctx, i) {
          return AvatarWidget(
            user: _users[i],
            onTap: () => _onUserStoryTap(context, i),
            isLarge: true,
            isShowingUsernameLabel: true,
            isCurrentUserStory: i == 0,
          );
        },
        itemCount: _users.length,
      ),
    );
  }
}
