import 'package:flutter/material.dart';
import './home.dart';
import './profile.dart';
import './splash.dart';
import './login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //DEFAULT YANG DI-LOAD ADALAH HALAMAN PROFILE
    return MaterialApp(
       debugShowCheckedModeBanner: false,
      //DEFINISIKAN ROUTENYA
      routes: {
        '/': (context) => SplashScreen(),
        '/login':(context)=> LoginPage(),
        '/home':(context)=> Home(),
        '/content':(context)=> Profile(),
      },
    );
  }
}
