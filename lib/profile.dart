import 'package:flutter/material.dart';
import './post.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            LastViewed(),
            ProfileInformation(),
            ProfileDescription(),
            Divider(),
            ListPosts(
                tabController: _tabController),
          ],
        ),
      ),
    );
  }
}

class ListPosts extends StatelessWidget {
  const ListPosts({
    Key key,
    @required TabController tabController,
  })  : _tabController = tabController,
        super(key: key);

  final TabController _tabController;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: <Widget>[
          //bagian TabBar
          TabBar(
            controller: _tabController,
            labelColor: Colors.black,
            indicatorColor: Colors.black,
            unselectedLabelColor: Colors.grey,
            //Ini buat kasih tau tabnya ada apa aja
            tabs: <Widget>[
              Tab(icon: Icon(Icons.border_all)),
              Tab(icon: Icon(Icons.assignment_ind))
            ],
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: <Widget>[
                GridView.count(
                  physics: ScrollPhysics(),
                  crossAxisCount: 3, 
                  childAspectRatio: 1.0,
                  mainAxisSpacing: 3.0, 
                  crossAxisSpacing: 3.0, 
                  children: <String>[
                    'assets/images/accProfile.jpg',
                    'assets/images/mina.jpg',
                    'assets/images/hanan.jpg',
                    'assets/images/nako.jpg'
                  ].map((String url) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => Post(url)));
                      },
                      child: GridTile(
                        child: Hero(
                            tag: url,
                            child: Image.asset(url, fit: BoxFit.cover)),
                      ),
                    );
                  }).toList(),
                ),
               GridView.count(
                  physics: ScrollPhysics(),
                  crossAxisCount: 3, 
                  childAspectRatio: 1.0,
                  mainAxisSpacing: 3.0, 
                  crossAxisSpacing: 3.0, 
                  children: <String>[
                    'assets/images/tie.jpg',
                    'assets/images/robertoPost.jpg',
                    'assets/images/hanan.jpg',
                  ].map((String url) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => Post(url)));
                      },
                      child: GridTile(
                        child: Hero(
                            tag: url,
                            child: Image.asset(url, fit: BoxFit.cover)),
                      )
                    );
                  }).toList(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ProfileDescription extends StatelessWidget {
  const ProfileDescription({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "PT. Accelist Lentera Indonesia",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
          Text(
            "Computer Company",
            style: TextStyle(color: Colors.grey),
          ),
          Text(
            "IT and Aviation Company\n\nAccelist Streams:",
          ),
          Text(
            "@accelistgamedev\nwww.accelist.com",
            style: TextStyle(color: Colors.blue),
          ),
        ],
      ),
    );
  }
}

class ProfileInformation extends StatelessWidget {
  const ProfileInformation({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: 100,
            height: 100,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Image.asset(
                'assets/images/accProfile.jpg',
                fit: BoxFit.cover,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Text(
                '3',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Posts",
                style: TextStyle(color: Colors.black45),
              )
            ],
          ),
          Column(
            children: <Widget>[
              Text('10.4M',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Text("Followers", style: TextStyle(color: Colors.black45))
            ],
          ),
          Column(
            children: <Widget>[
              Text('123',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Text("Following", style: TextStyle(color: Colors.black45))
            ],
          ),
        ],
      ),
    );
  }
}

class LastViewed extends StatelessWidget {
  const LastViewed({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:
          40,
      decoration: BoxDecoration(
        border: Border.fromBorderSide(
          BorderSide(width: 1, color: Colors.black12),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('9999', style: TextStyle(fontWeight: FontWeight.bold)),
          Text(' profile visits in the last 7 days',
              style: TextStyle(color: Colors.grey)),
        ],
      ),
    );
  }
}
